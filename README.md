MQ extention for notification-core
==============================================

This extension provides the ability to send messages deferred

Usage
=====
 
* Install extention.
* Configure AMQPStreamConnection connection:
```yaml
PhpAmqpLib\Connection\AMQPStreamConnection:
      autowire: true
      arguments:
        - '%rabbit_host%'
        - '%rabbit_port%'
        - '%rabbit_login%'
        - '%rabbit_pass%'
```
* Configure MQClient:
```yaml
Splashx\NotificationQueueBundle\Driver\MQClient:
  arguments:
  - '%rabbit_queue%'
  - '%rabbit_consumer%'
  - '@PhpAmqpLib\Connection\AMQPStreamConnection'
```
* Configure NotificationManager Decoration with:
```yaml
Splashx\NotificationQueueBundle\Model\NotificationManagerDecorator:
  decorates: 'symfony_bro.notification_core.notification_manager'
  arguments: ['@Splashx\NotificationQueueBundle\Model\NotificationManagerDecorator.inner']
```    
* In yours NotificationBuilder use MQNotificationDecorator notification type with argument notification object you need to send deffered.
```php
return new MQNotificationDecorator(new TelegramNotification([
    'chat_id' => '276316291',
    'fullName' => $context->getUserChangedPost()->getFullName(),
    'postTitle' => $context->getPost()->getTitle(),
    'template' => $template->getTemplate(),
]));
```
* Configure comand with:
```yaml
splashx_notification_queue.command.mqread_queue_command:
  class: Splashx\NotificationQueueBundle\Command\MQReadQueueCommand
  arguments: ['@symfony_bro.notification_core.notification_manager', '@Splashx\NotificationQueueBundle\Driver\MQClient']
  tags:
  - { name: console.command }
```
* Listen notification queue with MQnotifications:readQueue
* Be happy!
