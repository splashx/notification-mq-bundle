<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Splashx
 * Date: 10.09.2018
 * Time: 23:46
 */

namespace Tests\Splashx\Driver;

use Splashx\NotificationQueueBundle\Driver\MQClientInterface;
use Splashx\NotificationQueueBundle\Driver\MQDriver;
use Splashx\NotificationQueueBundle\Test\TestNotificationMock;
use PHPUnit\Framework\TestCase;
use SymfonyBro\NotificationCore\Model\MessageInterface;

class MQDriverTest extends TestCase
{
    public function testDoSend()
    {
        $MQClientInterface = $this->createMock(MQClientInterface::class);
        $testStrData = 'test_data_at_'.microtime();
        $notification = new TestNotificationMock($testStrData);
        $message = $this->createMock(MessageInterface::class);
        $message->method('getNotification')
                ->willReturn($notification);

        $MQClientInterface->expects($this->once())
                          ->method('sendQueue')
                          ->with($this->stringContains($testStrData));

        $driver = new MQDriver($MQClientInterface);
        $driver->send($message);
    }
}
