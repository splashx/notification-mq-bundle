<?php

/**
 * Created by PhpStorm.
 * User: Splashx
 * Date: 10.09.2018
 * Time: 23:46
 */

namespace Tests\Splashx\Driver;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PHPUnit_Framework_MockObject_MockObject;
use Splashx\NotificationQueueBundle\Driver\MQClient;
use Splashx\NotificationQueueBundle\Test\TestNotificationMock;
use PHPUnit\Framework\TestCase;
use SymfonyBro\NotificationCore\Model\NotificationManagerInterface;


class MQClientTest extends TestCase
{
    public function testSendQueue()
    {
        /** @var AMQPStreamConnection|PHPUnit_Framework_MockObject_MockObject $AMQPChannel */
        $AMQPChannel = $this->createMock(AMQPChannel::class);
        /** @var AMQPStreamConnection|PHPUnit_Framework_MockObject_MockObject $AMQPStreamConnection */
        $AMQPStreamConnection = $this->createMock(AMQPStreamConnection::class);
        $AMQPStreamConnection->method('channel')
                             ->willReturn($AMQPChannel);

        $AMQPChannel->expects($this->once())
                    ->method('basic_publish');

        $client = new MQClient('test', 'test', $AMQPStreamConnection);
        $client->sendQueue('test');
    }

    public function testListen()
    {
        /** @var AMQPStreamConnection|PHPUnit_Framework_MockObject_MockObject $AMQPChannel */
        $AMQPChannel = $this->createMock(AMQPChannel::class);

        /** @var AMQPStreamConnection|PHPUnit_Framework_MockObject_MockObject $AMQPStreamConnection */
        $AMQPStreamConnection = $this->createMock(AMQPStreamConnection::class);
        $AMQPStreamConnection->method('channel')
                             ->willReturn($AMQPChannel);

        /** @var NotificationManagerInterface|PHPUnit_Framework_MockObject_MockObject $notificator */
        $notificator = $this->getMockForAbstractClass(NotificationManagerInterface::class);
        $notificator->method('notify')
                    ->willReturn($this->returnArgument(0));

        $AMQPChannel->expects($this->once())
                    ->method('basic_consume');

        $client = new MQClient('test', 'test', $AMQPStreamConnection);
        $client->listen($notificator);

        $notification = new TestNotificationMock('test_data');

        $notificator->expects($this->once())
                    ->method('notify')
                    ->willReturn($notification);

        $messageData = [
            'notificationClass' => TestNotificationMock::class,
            'notification' => $notification->getSerialized(),
        ];

        $AMQPMessage = $this->createMock(AMQPMessage::class);
        $AMQPMessage->delivery_info = [
            'channel' => $AMQPChannel,
            'delivery_tag' => 'test',
        ];

        $AMQPMessage->method('getBody')
                    ->willReturn(json_encode($messageData));

        $client->process($AMQPMessage);
    }
}
