<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Splashx
 * Date: 10.09.2018
 * Time: 23:46
 */

namespace Tests\Splashx\Model;

use Splashx\NotificationQueueBundle\Driver\MQDriver;
use Splashx\NotificationQueueBundle\Driver\MQNotificationDecorator;
use Splashx\NotificationQueueBundle\Model\NotificationManagerDecorator;
use PHPUnit\Framework\TestCase;
use SymfonyBro\NotificationCore\Model\NotificationInterface;
use SymfonyBro\NotificationCore\Model\NotificationManagerInterface;

class NotificationManagerDecoratorTest extends TestCase
{
    public function testSimpleNotify()
    {
        $NotificationManagerInterface = $this->getMockForAbstractClass(NotificationManagerInterface::class);
        $MQDriver = $this->createMock(MQDriver::class);
        $decorNotificator = new NotificationManagerDecorator($NotificationManagerInterface, $MQDriver);

        $notification = $this->getMockForAbstractClass(NotificationInterface::class);
        $NotificationManagerInterface->expects($this->once())
                                     ->method('notify')
                                     ->with($notification);

        $MQDriver->expects($this->exactly(0))
                 ->method('send');

        $decorNotificator->notify($notification);
    }

    public function testMQNotify()
    {
        $NotificationManagerInterface = $this->getMockForAbstractClass(NotificationManagerInterface::class);
        $MQDriver = $this->createMock(MQDriver::class);
        $decorNotificator = new NotificationManagerDecorator($NotificationManagerInterface, $MQDriver);

        $notification = $this->createMock(MQNotificationDecorator::class);
        $NotificationManagerInterface->expects($this->exactly(0))
                                     ->method('notify')
                                     ->with($notification);

        $MQDriver->expects($this->once())
                 ->method('send');

        $decorNotificator->notify($notification);
    }
}
