<?php

/**
 * Created by PhpStorm.
 * User: splashx
 * Date: 13.09.2018
 * Time: 10:26
 */

namespace Splashx\NotificationQueueBundle\Model;

use Splashx\NotificationQueueBundle\Driver\MQDriver;
use Splashx\NotificationQueueBundle\Driver\MQMessage;
use Splashx\NotificationQueueBundle\Driver\MQNotificationDecorator;
use SymfonyBro\NotificationCore\Exception\NotificationException;
use SymfonyBro\NotificationCore\Model\NotificationInterface;
use SymfonyBro\NotificationCore\Model\NotificationManagerInterface;

class NotificationManagerDecorator implements NotificationManagerInterface
{
    /**
     * @var NotificationManagerInterface
     */
    private $originalManager;

    /**
     * @var MQDriver
     */
    private $driver;

    /**
     * NotificationManagerDecorator constructor.
     *
     * @param NotificationManagerInterface $originalManager
     * @param MQDriver                     $driver
     */
    public function __construct(NotificationManagerInterface $originalManager, MQDriver $driver)
    {
        $this->originalManager = $originalManager;
        $this->driver = $driver;
    }

    /**
     * @param NotificationInterface $notification
     *
     * @throws NotificationException
     */
    public function notify(NotificationInterface $notification)
    {
        if ($notification instanceof MQNotificationDecorator) {
            $this->driver->send(new MQMessage($notification));
        } else {
            $this->originalManager->notify($notification);
        }
    }
}
