<?php

/**
 * Created by PhpStorm.
 * User: splashx
 * Date: 13.09.2018
 * Time: 10:26
 */

namespace Splashx\NotificationQueueBundle\Model;

interface MQSerializableInterface
{
    public function getSerialized(): string;

    public static function unserialize(string $serialized);
}
