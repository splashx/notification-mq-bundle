<?php

/**
 * Created by PhpStorm.
 * User: splashx
 * Date: 13.09.2018
 * Time: 10:26
 */

namespace Splashx\NotificationQueueBundle\Command;

use Splashx\NotificationQueueBundle\Driver\MQClientInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use SymfonyBro\NotificationCore\Model\NotificationManagerInterface;

class MQReadQueueCommand extends Command
{
    /**
     * @var MQClientInterface
     */
    private $mqClient;

    private $notificator;

    public function __construct(NotificationManagerInterface $notificator, MQClientInterface $mqClient)
    {
        $this->notificator = $notificator;
        $this->mqClient = $mqClient;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('MQnotifications:readQueue')
            ->setDescription('Process notification from queue')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->mqClient->listen($this->notificator);
    }
}
