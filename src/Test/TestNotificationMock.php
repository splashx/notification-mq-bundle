<?php declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Splashx
 * Date: 10.09.2018
 * Time: 23:46
 */

namespace Splashx\NotificationQueueBundle\Test;

use Splashx\NotificationQueueBundle\Model\MQSerializableInterface;
use SymfonyBro\NotificationCore\Model\NotificationInterface;


class TestNotificationMock implements NotificationInterface, MQSerializableInterface
{
    private $data;

    /**
     * TestNotification constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }


    public function getSerialized(): string
    {
        return json_encode($this->data);
    }

    public static function unserialize(string $serialized)
    {
        return new self(json_decode($serialized));
    }
}
