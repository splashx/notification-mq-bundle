<?php

/**
 * Created by PhpStorm.
 * User: splashx
 * Date: 13.09.2018
 * Time: 10:26
 */

namespace Splashx\NotificationQueueBundle\Driver;

use SymfonyBro\NotificationCore\Model\NotificationInterface;

class MQNotificationDecorator implements NotificationInterface
{
    /**
     * @var NotificationInterface
     */
    private $notification;

    /**
     * MQNotificationDecorator constructor.
     *
     * @param NotificationInterface $notification
     */
    public function __construct(NotificationInterface $notification)
    {
        $this->notification = $notification;
    }

    /**
     * @return NotificationInterface
     */
    public function getNotification(): NotificationInterface
    {
        return $this->notification;
    }
}
