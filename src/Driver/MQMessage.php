<?php

/**
 * Created by PhpStorm.
 * User: splashx
 * Date: 13.09.2018
 * Time: 10:26
 */

namespace Splashx\NotificationQueueBundle\Driver;

use SymfonyBro\NotificationCore\Model\AbstractMessage;
use SymfonyBro\NotificationCore\Model\NotificationInterface;

class MQMessage extends AbstractMessage
{
    /**
     * MQMessage constructor.
     *
     * @param MQNotificationDecorator|NotificationInterface $notification
     */
    public function __construct(MQNotificationDecorator $notification)
    {
        parent::__construct($notification->getNotification());
    }
}
