<?php

/**
 * Created by PhpStorm.
 * User: splashx
 * Date: 13.09.2018
 * Time: 10:26
 */

namespace Splashx\NotificationQueueBundle\Driver;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use SymfonyBro\NotificationCore\Model\NotificationInterface;
use SymfonyBro\NotificationCore\Model\NotificationManagerInterface;

class MQClient implements MQClientInterface
{
    protected const EXCHANGE = 'router';

    /**
     * @var AMQPStreamConnection
     */
    private $connection;

    /**
     * @var AMQPChannel
     */
    private $channel;

    /**
     * @var string
     */
    private $QUEUE;
    /**
     * @var string
     */
    private $CONSUMERTAG;

    /**
     * @var NotificationManagerInterface
     */
    private $notificationManager;

    /**
     * MQClient constructor.
     *
     * @param string               $QUEUE
     * @param string               $CONSUMERTAG
     * @param AMQPStreamConnection $AMQPStreamConnection
     */
    public function __construct(string $QUEUE, string $CONSUMERTAG, AMQPStreamConnection $AMQPStreamConnection)
    {
        $this->connection = $AMQPStreamConnection;
        $this->QUEUE = $QUEUE;
        $this->CONSUMERTAG = $CONSUMERTAG;
    }

    /**
     * @return AMQPStreamConnection
     */
    public function getConnection(): AMQPStreamConnection
    {
        return $this->connection;
    }

    /**
     * @return AMQPChannel
     */
    public function getChannel(): AMQPChannel
    {
        if (null === $this->channel) {
            $this->channel = $this->connection->channel();

            $this->channel->queue_declare($this->QUEUE, false, true, false, false);
            $this->channel->exchange_declare(self::EXCHANGE, 'direct', false, true, false);
            $this->channel->queue_bind($this->QUEUE, self::EXCHANGE);
        }

        return $this->channel;
    }

    public function sendQueue(string $data)
    {
        $message = new AMQPMessage($data, ['content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);
        $this->getChannel()->basic_publish($message, self::EXCHANGE);
    }

    public function process(AMQPMessage $message)
    {
        $data = json_decode($message->getBody());

        /** @var NotificationInterface $notification */
        $notification = ($data->notificationClass)::unserialize($data->notification);
        $this->notificationManager->notify($notification);

        $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
    }

    public function listen(NotificationManagerInterface $notificator)
    {
        $this->notificationManager = $notificator;
        $this->getChannel()->basic_consume($this->QUEUE, $this->CONSUMERTAG, false, false, false, false, [$this, 'process']);

        while (\count($this->getChannel()->callbacks)) {
            $this->getChannel()->wait();
        }
    }
}
