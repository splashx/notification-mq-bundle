<?php

/**
 * Created by PhpStorm.
 * User: splashx
 * Date: 13.09.2018
 * Time: 10:26
 */

namespace Splashx\NotificationQueueBundle\Driver;

use SymfonyBro\NotificationCore\Model\NotificationManagerInterface;

interface MQClientInterface
{
    public function sendQueue(string $data);

    public function listen(NotificationManagerInterface $notificator);
}
