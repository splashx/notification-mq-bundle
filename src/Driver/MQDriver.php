<?php

/**
 * Created by PhpStorm.
 * User: splashx
 * Date: 13.09.2018
 * Time: 10:26
 */

namespace Splashx\NotificationQueueBundle\Driver;

use Splashx\NotificationQueueBundle\Model\MQSerializableInterface;
use SymfonyBro\NotificationCore\Model\AbstractDriver;
use SymfonyBro\NotificationCore\Model\MessageInterface;

class MQDriver extends AbstractDriver
{
    /**
     * @var MQClientInterface
     */
    private $mqClient;

    /**
     * MQDriver constructor.
     *
     * @param MQClientInterface $mqClient
     */
    public function __construct(MQClientInterface $mqClient)
    {
        $this->mqClient = $mqClient;
    }

    /**
     * @param MQMessage|MessageInterface $message *
     *
     * @throws \Exception
     */
    protected function doSend(MessageInterface $message)
    {
        $notification = $message->getNotification();
        if (!$notification instanceof MQSerializableInterface) {
            throw new \Exception('Notification not implements MQSerializableInterface interface');
        }

        /** @var MQSerializableInterface $notification */
        $data = [
            'notification' => $notification->getSerialized(),
            'notificationClass' => \get_class($notification),
        ];

        $this->mqClient->sendQueue(json_encode($data));
    }
}
